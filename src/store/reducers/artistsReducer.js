import {
    FETCH_ARTISTS_SUCCESS,
    FETCH_ARTISTS_ERROR,
    CREATE_ARTISTS_ERROR,
    DELETE_ARTISTS_SUCCESS,
    PUBLISH_ARTISTS_SUCCESS
}
    from "../actions/actionTypes";

const initialState = {
    error: null,
    artists:[],
    published: false
};

const artistsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ARTISTS_SUCCESS:
            return {...state, artists: action.artistsList};
        case FETCH_ARTISTS_ERROR:
            return {...state, error: action.error};
        case PUBLISH_ARTISTS_SUCCESS:
            return {...state, published: true};
        case CREATE_ARTISTS_ERROR:
            return {...state, error: action.error}
        case DELETE_ARTISTS_SUCCESS:
            const newArtist = {...state.artists};
            const id = newArtist.findIndex(p => p.id === action.id);
            newArtist.splice(id, 1);
            return {...state, artists : {...newArtist}};
        default:
            return state;
    }
};

export default artistsReducer;