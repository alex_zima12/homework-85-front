import {
    FETCH_TRACKHISTORY_SUCCESS,
    ADD_TRACKTOTRACKHISTORY_ERROR,
    FETCH_TRACKHISTORY_ERROR
}
    from "../actions/actionTypes";

const initialState = {
    error: null,
    trackHistory:[]

};

const trackHistoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TRACKHISTORY_SUCCESS:
            return {...state, trackHistory: action.tracksList};
        case ADD_TRACKTOTRACKHISTORY_ERROR:
            return {...state, error: action.error};
        case FETCH_TRACKHISTORY_ERROR:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default trackHistoryReducer;