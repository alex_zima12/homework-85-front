import {
    FETCH_TRACKS_SUCCESS,
    FETCH_TRACKS_ERROR,
    CREATE_TRACK_ERROR,
    DELETE_TRACK_SUCCESS
}
    from "../actions/actionTypes";

const initialState = {
    error: null,
    tracks:[]
};

const tracksReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TRACKS_ERROR:
            return {...state, error: action.error};
        case FETCH_TRACKS_SUCCESS:
            return {...state, tracks: action.trackList};
        case CREATE_TRACK_ERROR:
            return {...state, error: action.error};
        case DELETE_TRACK_SUCCESS:
            const newTrack = {...state.tracks};
            const id = newTrack.findIndex(p => p.id === action.id);
            newTrack.splice(id, 1);
            return {...state, tracks : {...newTrack}};
        default:
            return state;
    }
};

export default tracksReducer;