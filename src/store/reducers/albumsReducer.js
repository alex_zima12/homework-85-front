import {
    FETCH_ALBUMS_SUCCESS,
    FETCH_ALBUMS_ERROR,
    CREATE_ALBUM_ERROR,
    DELETE_ALBUM_SUCCESS
}
    from "../actions/actionTypes";

const initialState = {
    error: null,
    albums:[]
};

const albumsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALBUMS_SUCCESS:
            return {...state, albums: action.albumsList};
        case FETCH_ALBUMS_ERROR:
            return {...state, error: action.error}
        case CREATE_ALBUM_ERROR:
            return {...state, error: action.error}
        case DELETE_ALBUM_SUCCESS:
            const newAlbums = {...state.albums};
            const id = newAlbums.findIndex(p => p.id === action.id);
            newAlbums.splice(id, 1);
            return {...state, albums : {...newAlbums}};
        default:
            return state;
    }
};

export default albumsReducer;