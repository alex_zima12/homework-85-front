import {
    FETCH_TRACKS_SUCCESS,
    FETCH_TRACKS_ERROR,
    CREATE_TRACK_SUCCESS,
    CREATE_TRACK_ERROR,
    PUBLISH_TRACK_SUCCESS,
    DELETE_TRACK_SUCCESS
} from "./actionTypes";
import axios from "../../axiosApi";

const fetchTracksSuccess = (trackList) => {
    return {type: FETCH_TRACKS_SUCCESS, trackList}
};

const fetchTracksError = (error) => {
    return {type: FETCH_TRACKS_ERROR, error}
};

export const fetchTracks = (id) => {
    return async dispatch => {
        try {
            const response = await axios.get("/tracks?album=" + id);
            dispatch(fetchTracksSuccess(response.data));
        } catch (e) {
            dispatch(fetchTracksError(e.response.data.error));
        }
    };
};

const createNewTrackError = (error) => {
    return {type: CREATE_TRACK_ERROR, error}
};

export const createNewTrack = data => {
    return async dispatch => {
        try {
            await axios.post('/tracks', data);
            dispatch({type: CREATE_TRACK_SUCCESS});
            alert("Успешно добавленно")
        } catch (e) {
            dispatch(createNewTrackError(e.response.data.error));
        }
    };
}

export const publishTrack = id => {
    return async dispatch => {
        try {
            await axios.put('/tracks/'+ id +'/publish', {published:true});
            dispatch({type: PUBLISH_TRACK_SUCCESS});
        } catch (e) {
            // dispatch(createNewArtistError(e.response.data.error));
        }
    };
}

const deleteTrackSuccess = (id) => {
    return {type: DELETE_TRACK_SUCCESS, id}
};

export const deleteTrack = id => {
    return async dispatch => {
        try {
            await axios.delete('/tracks/'+ id);
            dispatch(deleteTrackSuccess(id));
        } catch (e) {
            // dispatch(createNewArtistError(e.response.data.error));
        }
    };
}