import {
    FETCH_ALBUMS_SUCCESS,
    FETCH_ALBUMS_ERROR,
    CREATE_ALBUM_SUCCESS,
    CREATE_ALBUM_ERROR,
    PUBLISH_ALBUM_SUCCESS,
    DELETE_ALBUM_SUCCESS
} from "./actionTypes";
import axios from "../../axiosApi";

const fetchAlbumsSuccess = (albumsList) => {
    return {type: FETCH_ALBUMS_SUCCESS, albumsList}
};

const fetchAlbumsError = (error) => {
    return {type: FETCH_ALBUMS_ERROR, error}
};

export const fetchAlbum = (id) => {
    return async dispatch => {
        try {
        const response = await axios.get("/albums?artist=" + id);
        dispatch(fetchAlbumsSuccess(response.data));
        } catch (e) {
            dispatch(fetchAlbumsError(e.response.data.error));
        }

    };
};

const createNewAlbumError = (error) => {
    return {type: CREATE_ALBUM_ERROR, error}
};

export const createNewAlbum = data => {
    return async dispatch => {
        try {
            await axios.post('/albums', data);
            dispatch({type: CREATE_ALBUM_SUCCESS});
            alert("Успешно добавленно")
        } catch (e) {
            dispatch(createNewAlbumError(e.response.data.error));
        }
    };
}

export const publishAlbum = id => {
    return async dispatch => {
        try {
            await axios.put('/albums/'+ id +'/publish', {published:true});
            dispatch({type: PUBLISH_ALBUM_SUCCESS});
        } catch (e) {
            // dispatch(createNewArtistError(e.response.data.error));
        }
    };
}

const deleteAlbumSuccess = (id) => {
    return {type: DELETE_ALBUM_SUCCESS, id}
};

export const deleteAlbum = id => {
    return async dispatch => {
        try {
            await axios.delete('/albums/'+ id);
            dispatch(deleteAlbumSuccess(id));
        } catch (e) {
            // dispatch(createNewArtistError(e.response.data.error));
        }
    };
}