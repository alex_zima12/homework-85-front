import {
    FETCH_ARTISTS_SUCCESS,
    FETCH_ARTISTS_ERROR,
    CREATE_ARTISTS_SUCCESS,
    CREATE_ARTISTS_ERROR,
    PUBLISH_ARTISTS_SUCCESS,
    DELETE_ARTISTS_SUCCESS
} from "./actionTypes";
import axios from "../../axiosApi";

const fetchArtistsSuccess = (artistsList) => {
    return {type: FETCH_ARTISTS_SUCCESS, artistsList}
};

const fetchArtistsError = (error) => {
    return {type: FETCH_ARTISTS_ERROR, error}
};

export const fetchArtists = () => {
    return async dispatch => {
        try {
        const response = await axios.get("/artists");
        dispatch(fetchArtistsSuccess(response.data));
        } catch (e) {
            dispatch(fetchArtistsError(e.response.data.error));
        }
    };
};

const createNewArtistError = (error) => {
    return {type: CREATE_ARTISTS_ERROR, error}
};

export const createNewArtist = data => {
    return async dispatch => {
        try {
            await axios.post('/artists', data);
            dispatch({type: CREATE_ARTISTS_SUCCESS});
            alert("Успешно добавленно")
        } catch (e) {
            dispatch(createNewArtistError(e.response.data.error));
        }
    };
}

const publishArtistSuccess  = () => {
    return {type: PUBLISH_ARTISTS_SUCCESS}
};

export const publishArtist = id => {
    return async dispatch => {
        try {
           const response = await axios.put('/artists/'+ id +'/publish', {published:true});
            console.log(response.data,"bxfdbd");
            dispatch(publishArtistSuccess);
        } catch (e) {
            // dispatch(createNewArtistError(e.response.data.error));
        }
    };
}

const deleteArtistSuccess = (id) => {
    return {type: DELETE_ARTISTS_SUCCESS, id}
};

export const deleteArtist = id => {
    return async dispatch => {
        try {
            await axios.delete('/artists/'+ id);
            dispatch(deleteArtistSuccess(id));

        } catch (e) {
            // dispatch(createNewArtistError(e.response.data.error));
        }
    };
}