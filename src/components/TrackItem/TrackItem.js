import React from 'react';
import {useSelector} from "react-redux";
import {useDispatch} from "react-redux";
import {addTrackToTrackHistory} from "../../store/actions/TrackHistoryActions";
import {publishTrack, deleteTrack} from "../../store/actions/TracksActions";


const TrackItem = ({title, trackNum, duration, id, published}) => {
    const user = useSelector(state => state.users.user);
    const dispatch = useDispatch();

    const publishItem = () => {
        dispatch(publishTrack(id));
    };

    const deleteItem = () => {
        dispatch(deleteTrack(id));
    };

    const addToHistoryHandler = (id) => {
        dispatch(addTrackToTrackHistory(id));
    }



    return (
        <li className="list-group-item">
            <p>{trackNum}</p>
            <h3 className="card-title">{title}</h3>
            <p>{duration}</p>
            {user ? <button id="btn-test" onClick={() => addToHistoryHandler(id)}
                >Add to my track history</button>
                : null}
            {user && user.role === "admin" ?
                <>
                    {published === false ?
                        <> <p className="text-danger text-center border"> The item is not published</p>
                            <button onClick={publishItem}> Publish</button>
                        </> :
                        null
                    }
                    <button className="text-danger mt-3" onClick={deleteItem}> Delete</button>
                </>
                : null
            }
        </li>
    );
};

export default TrackItem;