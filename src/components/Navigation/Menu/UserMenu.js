import React from "react";
import {NavLink} from "react-router-dom";
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../store/actions/UsersActions";
import defaultAvatar from "../../../assets/images/SD-default-image.png";

const mystyle = {
    width: "50px",
    height:"50px"
};

const UserMenu = ({user}) => {
    console.log(user.user.user);

    const dispatch = useDispatch();

    const logout = () => {
        dispatch(logoutUser());
    };

    return user && (
        <>
            <div>
                <NavLink className="font-weight-bold text-light m-3" id="new_artist" to={"/add/new_artist"}>Add New Artist</NavLink>
                <NavLink className="font-weight-bold text-light m-3" id="new_album" to={"/add/new_album"}>Add New Album</NavLink>
                <NavLink className="font-weight-bold text-light m-3" id="new_track" to={"/add/new_track"}>Add New Track</NavLink>
            </div>
            <div>
                <NavLink className="font-weight-bold text-light " to={"/trackHistory"}>My Track History</NavLink>
            </div>
            <div>
                <div className="font-weight-bold text-light ">Hello, {user.user.user.displayName || user.user.user.username}!
                    <img src={user.user.user.avatarImage ? user.user.user.avatarImage : defaultAvatar }
                         className="rounded circle rounded-circle m-3"
                         style={mystyle}
                         alt="user"
                    />
                </div>
                <button className="font-weight-bold" onClick={logout}>Logout</button>
            </div>
        </>
);
};

export default UserMenu;