import React from 'react';
import {NavLink} from 'react-router-dom';
import UserMenu from "../Menu/UserMenu";
import AnonymousMenu from "../Menu/AnonymousMenu";

const Toolbar = (user) => {
    return (
        <>
            <nav className="navbar navbar-dark bg-primary row">
                <NavLink className="font-weight-bold text-light text-decoration-none display-4 ml-3" to={"/"}>
                    Music Catalog App</NavLink>
            </nav>
            <nav className="navbar navbar-dark bg-primary row">
                <div className="container">
                    {user.user == null ?
                        <AnonymousMenu/>
                        :
                        <UserMenu user={user}/>
                    }
                </div>
            </nav>
        </>
    );
};

export default Toolbar;