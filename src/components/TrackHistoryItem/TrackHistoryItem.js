import React from 'react';
import moment from 'moment';


const TrackHistoryItem = ({track, datetime, id}) => {
    let now = moment(datetime);
    let date = now.format("DD/MM/YYYY hh:mm:ss");
    let trackInfo;
    if (track) {
        trackInfo = <><h1>{track.title}</h1><p>{track.album.artist.title}</p><p>{date}</p></>
    }

    return track && (
        <li className="list-group-item" >
            {trackInfo}
        </li>
    );
};

export default TrackHistoryItem;