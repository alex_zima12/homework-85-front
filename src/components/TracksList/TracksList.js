import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchTracks} from "../../store/actions/TracksActions";
import TrackItem from "../TrackItem/TrackItem";

const TracksList = props => {
    const id = props.match.params.id;
    const dispatch = useDispatch();
    const tracks = useSelector(state => state.tracks.tracks);
    const error = useSelector(state => state.tracks.error);

    useEffect(() => {
        dispatch(fetchTracks(id));
    }, [dispatch,id]);

    let tracksInformation = [];
    console.log(tracks, "треки");
    tracks.map((track)=> {
        let trackCard = <TrackItem
            id={track._id}
            key={track._id}
            trackNum={track.trackNum}
            title={track.title}
            duration={track.duration}
            published={track.published}
        />;
         return tracksInformation.push(trackCard)
    });
    const pageTitle = [];
    tracks.map((track)=> {return pageTitle.push (<div key={track._id} className="mt-3"><h1>{track.album.artist.title}</h1><p>Album: {track.album.title}</p></div>)})
    return tracks && (
        <>
            {error ? <b>{error}</b> :
                <div className="container">
                    {pageTitle[0]}
                    <ul className="list-group">
                        {tracksInformation}
                    </ul>
                </div>
            }
        </>

    );
};

export default TracksList;