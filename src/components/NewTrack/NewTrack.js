import React from "react";
import {useDispatch} from "react-redux";
import {createNewTrack} from "../../store/actions/TracksActions";
import AddTrackForm from "../../containers/AddTrackForm/AddTrackForm";

const NewTrack = props => {
    const dispatch = useDispatch();

    const createTrack = data => {
        dispatch(createNewTrack(data)).then(() => {
            props.history.push("/");
        });
    };

    return (
        <div className="container">
            <h1 className="text-dark mt-3">Add artist</h1>
            <AddTrackForm onSubmit={createTrack} />
        </div>
    );
};

export default NewTrack;