import React from 'react';
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props';
import {useDispatch} from "react-redux";
import {facebookAppId} from "../../constans";
import {facebookLogin} from "../../store/actions/UsersActions";


const FacebookLogin = () => {
    const dispatch = useDispatch();

    const facebookResponse = response => {
        console.log(response, " c facebook");
        if (response.id) {
            dispatch(facebookLogin(response));
        }
    };

    return <FacebookLoginButton
        appId={facebookAppId}
        fields="name,email,picture"
        render={renderProps => (
            <button
                className="btn btn-primary"
                onClick={renderProps.onClick}>
                Enter with Facebook
            </button>
        )}
        callback={facebookResponse}
    />
}


export default FacebookLogin;