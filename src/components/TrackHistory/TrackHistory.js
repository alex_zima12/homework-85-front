import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {fetchTrackHistory} from "../../store/actions/TrackHistoryActions";
import {push} from "connected-react-router";
import TrackHistoryItem from "../TrackHistoryItem/TrackHistoryItem";

const TrackHistory = () => {
    const dispatch = useDispatch();
    const trackHistory = useSelector(state => state.trackHistory.trackHistory);
    const user = useSelector(state => state.users.user);
    const error = useSelector(state => state.trackHistory.error);

    useEffect(() => {
        if   (user) {
            dispatch(fetchTrackHistory());
        } else {
            dispatch(push('/login'));
        }
        }, [dispatch,user]);

    let tracksInformation = [];
    trackHistory.map((track) => {
        let trackCard = <TrackHistoryItem
            key={track._id}
            track={track.track}
            datetime={track.datetime}
        />;
        return tracksInformation.push(trackCard)
    });

    return trackHistory && (
        <>
            {error ? <b>{error}</b> :
                <div className="container mt-3">
                    {tracksInformation}
                </div>
            }
        </>
    );
};

export default TrackHistory;