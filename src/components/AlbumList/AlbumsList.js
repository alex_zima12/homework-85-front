import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchAlbum} from "../../store/actions/AlbumsActions";
import AlbumItem from "../AlbumItem/AlbumItem";

const AlbumsList = props => {
    const id = props.match.params.id;
    const dispatch = useDispatch();
    const albums = useSelector(state => state.albums.albums);
    const error = useSelector(state => state.albums.error);
    console.log(albums);

    useEffect(() => {
        dispatch(fetchAlbum(id));
    }, [dispatch, id]);

    let albumsInformation = [];
    albums.map((album) => {
        let albumCard = <AlbumItem
            id={album._id}
            key={album._id}
            artist={album.artist}
            year={album.year}
            title={album.title}
            image={album.image}
            published={album.published}
        />;
        return albumsInformation.push(albumCard)
    });
    const name = []
    albums.map((album) => {
        return name.push(<h1 key={album._id}>{album.artist.title}</h1>)
    })

    return albums && (
        <>
            {error ? <b>{error}</b> :
                <div className="container mt-3">
                    {name[0]}
                    <div className="row">
                        {albumsInformation}
                    </div>
                </div>
            }
        </>
    );
};

export default AlbumsList;