import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchArtists} from "../../store/actions/ArtistsActions";
import ArtistsItem from "../ArtistsItem/ArtistsItem";

const ArtistsList = props => {
    const dispatch = useDispatch();
    const artists = useSelector(state => state.artists.artists);
    const error = useSelector(state => state.artists.error);
    console.log(artists);

    useEffect(() => {
        dispatch(fetchArtists());
    }, [dispatch]);

    let artistInformation = [];
    artists.map((artist)=> {
        let artistCard = <ArtistsItem
            id={artist._id}
            key={artist._id}
            title={artist.title}
            image={artist.image}
            published={artist.published}/>;
        return artistInformation.push(artistCard)
    });

    return artists && (
        <>
            {error ? <b>{error}</b> :
                <div className="container">
                    <div className="d-flex flex-wrap">
                        {artistInformation}
                    </div>
                </div>
            }
        </>
    );
};

export default ArtistsList;