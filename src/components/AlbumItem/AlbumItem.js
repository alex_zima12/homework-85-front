import React from 'react';
import {apiURL} from "../../constans";
import {NavLink} from 'react-router-dom';
import defaultImage from "../../assets/images/SD-default-image.png";
import {useDispatch, useSelector} from "react-redux";
import {publishAlbum, deleteAlbum} from "../../store/actions/AlbumsActions";

const AlbumItem = ({title, image, artist, year, id,published}) => {
    const user = useSelector(state => state.users.user);
    const dispatch = useDispatch();

    const  publishItem = () => {
        dispatch(publishAlbum(id));
    };

    const  deleteItem = () => {
        dispatch(deleteAlbum(id));
    };

    let cardImage = defaultImage;
    if (image) {
        cardImage =  apiURL + "/uploads/" + image;
    }

    return (
        <div className="card d-flex col-sm m-3 p-3">
            <h3 className="card-title">{artist.title}</h3>
            <h5 className="card-title  flex-md-grow-1 ">{title}</h5>
            <img src={cardImage} className="card-img-top flex-md-grow-1 " alt="Artist"/>
            <div className="card-body d-flex flex-column">
                <p className="flex-md-grow-1">{year}</p>
                <NavLink to={"/tracks/" + id} className="btn btn-primary"> See tracks</NavLink>
            </div>
            {user && user.role === "admin" ?
                <>
                    {published === false ?
                        <> <p className="text-danger text-center border"> The item is not published</p>
                            <button onClick={publishItem}> Publish</button>
                        </> :
                        null
                    }
                    <button className="text-danger mt-3" onClick={deleteItem}> Delete</button>
                </>
                : null
            }
        </div>

    );
};

export default AlbumItem;