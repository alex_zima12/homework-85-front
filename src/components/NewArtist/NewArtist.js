import React from "react";
import {useDispatch} from "react-redux";
import {createNewArtist} from "../../store/actions/ArtistsActions";
import AddArtistForm from "../../containers/AddArtistForm/AddArtistForm";

const NewArtist = props => {
    const dispatch = useDispatch();

    const createArtist = data => {
        dispatch(createNewArtist(data)).then(() => {
            props.history.push("/");
        });
    };

    return (
        <div className="container">
            <h1 className="text-dark mt-3">Add artist</h1>
            <AddArtistForm onSubmit={createArtist} />
        </div>
    );
};

export default NewArtist;