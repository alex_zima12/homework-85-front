import React from 'react';
import {apiURL} from "../../constans";
import {NavLink} from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import {publishArtist, deleteArtist} from "../../store/actions/ArtistsActions";
import defaultImage from "../../assets/images/SD-default-image.png";

const ArtistsItem = ({title, image, id, published}) => {
    const user = useSelector(state => state.users.user);
    const isPublished = useSelector(state => state.artists.published);
    const dispatch = useDispatch();
    console.log(user, "useeeeeer");
    const publishItem = () => {
        dispatch(publishArtist(id));
    };

    const deleteItem = () => {
        dispatch(deleteArtist(id));
    };

    let cardImage = defaultImage;
    if (image) {
        cardImage =  apiURL + "/uploads/" + image;
    }

     return (
        <div className="card col-sm m-3 p-3">
            <h5 className="card-title ">{title}</h5>
            <img src={cardImage} className="card-img-top flex-md-grow-1 " alt="Artist"/>
            <NavLink to={"/albums/" + id} className="btn btn-primary m-3"> More details</NavLink>
            {user && user.role === "admin" ?
                <>
                    {published === false  || isPublished === false ?
                        <> <p className="text-danger text-center border"> The item is not published</p>
                            <button onClick={publishItem}> Publish</button>
                        </> :
                        null
                    }
                    <button className="text-danger mt-3" onClick={deleteItem}> Delete</button>
                </>
                : null
            }
        </div>
    );
};

export default ArtistsItem;