import React from "react";
import {useDispatch} from "react-redux";
import {createNewAlbum} from "../../store/actions/AlbumsActions";
import AddAlbumForm from "../../containers/AddAlbumForm/AddAlbumForm";

const NewAlbum = props => {
    const dispatch = useDispatch();

    const createAlbum = data => {
        dispatch(createNewAlbum(data)).then(() => {
            props.history.push("/");
        });
    };

    return (
        <div className="container">
            <h1 className="text-dark mt-3">Add artist</h1>
            <AddAlbumForm onSubmit={createAlbum} />
        </div>
    );
};

export default NewAlbum;