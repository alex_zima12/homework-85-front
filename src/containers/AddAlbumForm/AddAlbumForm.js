import React, {useState} from "react";
import {useSelector} from "react-redux";

const AddAlbumForm = ({onSubmit}) => {
    const artistArr = useSelector(state => state.artists.artists);

    const [state, setState] = useState({
        title: "",
        artist: "",
        image: "",
        year:""
    });

    const submitFormHandler = e => {
        e.preventDefault();
        let formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });
        onSubmit(formData);
    };
    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };
    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => ({...prevState, [name]: file}));
    };

    return (
        <div className="container bg-gradient-info">
            <form
                className="p-5"
                onSubmit={submitFormHandler}
            >
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1">Album title</span>
                    </div>
                    <input type="text"
                           className="form-control"
                           id="title-album"
                           placeholder="Enter album title"
                           value={state.title}
                           onChange={inputChangeHandler}
                           name="title"
                           required/>
                </div>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1">Year</span>
                    </div>
                    <input type="text"
                           className="form-control"
                           id="year-album"
                           placeholder="This album was released in"
                           value={state.year}
                           onChange={inputChangeHandler}
                           name="year"
                           required/>
                </div>

                <select
                    className="browser-default custom-select"
                    name="artist"
                    value={state.artist}
                    onChange={inputChangeHandler}
                    id="option-artist"
                    required
                >
                    <option>Open this select menu to choose artist</option>
                    {artistArr.map(artist =>  <option
                        key={artist._id}
                        value={artist._id}
                    >{artist.title}
                    </option>
                   )}
                </select>

                <div className="input-group mb-3 ">
                    <label htmlFor="image">Add file</label>
                    <input
                        type="file"
                        name="image"
                        className="form-control-file"
                        id="image-album"
                        onChange={fileChangeHandler}
                    />
                </div>
                <button
                    type="submit"
                    className="btn btn-info"
                    id="btn-album"
                >Create new album
                </button>
            </form>
        </div>
    );
};

export default AddAlbumForm;