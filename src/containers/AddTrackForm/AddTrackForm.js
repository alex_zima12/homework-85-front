import React, {useState} from "react";
import {useSelector} from "react-redux";

const AddTrackForm = ({onSubmit}) => {
    const albumArr = useSelector(state => state.albums.albums)

    const [state, setState] = useState({
        title: "",
        album: "",
        duration: "",
        trackNum:""
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };
    const formSubmitHandler = e => {
        e.preventDefault();
        onSubmit({...state});
    }

    return (
        <div className="container bg-gradient-info">
            <form
                className="p-5"
                onSubmit={formSubmitHandler}
            >
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1">Title Track</span>
                    </div>
                    <input type="text"
                           className="form-control"
                           id="title-track"
                           placeholder="Enter track title"
                           value={state.title}
                           onChange={inputChangeHandler}
                           name="title"
                           required/>
                </div>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text">Duration</span>
                    </div>
                    <textarea className="form-control"
                              placeholder="How long is a running track?"
                              onChange={inputChangeHandler}
                              name="duration"
                              id="duration-tack"
                              value={state.duration}
                              required
                    />
                </div>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text">Number</span>
                    </div>
                    <textarea className="form-control"
                              placeholder="What is a track number? "
                              onChange={inputChangeHandler}
                              name="trackNum"
                              id="trackNum-tack"
                              value={state.trackNum}
                              required
                    />
                </div>

                <select
                    className="browser-default custom-select"
                    name="album"
                    id="option-album"
                    value={state.album}
                    onChange={inputChangeHandler}
                    required
                >
                    <option>Open this select menu to choose album</option>
                    {albumArr.map(album => <option
                        key={album._id}
                        value={album._id}
                    >{album.title}
                    </option>)}
                </select>
                <button
                    type="submit"
                    className="btn btn-info"
                    id="btn-track"
                >Create new track
                </button>
            </form>
        </div>
    );
};

export default AddTrackForm;