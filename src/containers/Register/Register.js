import React, {useState} from 'react';
import {useDispatch,useSelector} from "react-redux";
import {registerUser} from "../../store/actions/UsersActions";
import {NavLink} from "react-router-dom";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

const Register = () => {
    const [state, setState] = useState({
        username: "",
        password: "",
        displayName: "",
        avatarImage: ""
    });

    const dispatch = useDispatch();
    const error = useSelector(state => state.users.registerError);


    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const submitFormHandler = e => {
        e.preventDefault();
        let formData = new FormData();
        console.log(state, "state");
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });
        dispatch(registerUser(formData));
    };
    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => ({...prevState, [name]: file}));
    };

    let errorMes;
    if (error){
        errorMes = <div className="text-danger mb-3">{error.message}</div>
    }

    return  (
        <form
            onSubmit={submitFormHandler}
            className="container mt-3"
        >
            <div className="form-group">
                <label htmlFor="InputName">Login</label>
                <input type="text"
                       className="form-control"
                       id="InputName"
                       placeholder="Enter your login"
                       name="username"
                       value={state.username}
                       onChange={inputChangeHandler}
                       required
                />
            </div>
            <div className="form-group">
                <label htmlFor="InputDisplayName">Name</label>
                <input type="text"
                       className="form-control"
                       id="InputDisplayName"
                       placeholder="Adding your Display Name on Music App is a great way to help friends recognize you, login aren't always so easy to understand"
                       name="displayName"
                       value={state.displayName}
                       onChange={inputChangeHandler}
                       required
                />
            </div>
            <div className="form-group">
                <label htmlFor="InputPassword">Password</label>
                <input type="password"
                       className="form-control"
                       id="InputPassword1"
                       placeholder="Password"
                       name="password"
                       value={state.password}
                       onChange={inputChangeHandler}
                       required
                />
            </div>
            <div className="form-group mb-3 ">
                <label htmlFor="FormAvatarImage">You can add a photo to set as your profile picture </label>
                <input
                    type="file"
                    name="avatarImage"
                    className="form-control-file"
                    id="FormAvatarImage"
                    onChange={fileChangeHandler}
                />
            </div>
            {errorMes}
            <button type="submit" className="btn btn-primary"> Sign Up</button>
            <div className="mt-3">
            <FacebookLogin/>
            </div>
            <div className="mt-3">
            <NavLink to="/login" >
                Already have an account? Sign in
            </NavLink>
            </div>
        </form>
    );
};

export default Register;