import React, {useState} from 'react';
import {useDispatch,useSelector} from "react-redux";
import {loginUser} from "../../store/actions/UsersActions";
import {NavLink} from "react-router-dom";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

const Login = () => {
    const [state, setState] = useState({
        username: "",
        password: ""
    });
    const dispatch = useDispatch();
    const error = useSelector(state => state.users.loginError);

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };
    const formSubmitHandler = e => {
        e.preventDefault();
        dispatch(loginUser({...state}));
    }

    let errorMes;
    if (error){
        errorMes = <div className="text-danger mb-3">{error.error}</div>
    }

    return  (
        <form
            onSubmit={formSubmitHandler}
            className="container mt-3"
        >
            <div className="form-group">
                <label htmlFor="username">Name</label>
                <input type="text"
                       className="form-control"
                       id="username"
                       placeholder="Enter your name"
                       name="username"
                       value={state.username}
                       onChange={inputChangeHandler}
                       required
                />

            </div>
            <div className="form-group">
                <label htmlFor="password">Password</label>
                <input type="password"
                       className="form-control"
                       id="password"
                       placeholder="Password"
                       name="password"
                       value={state.password}
                       onChange={inputChangeHandler}
                       required
                />
            </div>
            {errorMes}
            <button type="submit"
                    className="btn btn-primary"
                    id="loginBtn"
            > Sign In</button>
            <div className="mt-3">
            <FacebookLogin/>
            </div>
            <div className="mt-3">
            <NavLink to="/register"  >
                Sign up for free to start listening
            </NavLink>
            </div>
        </form>
    );
};

export default Login;