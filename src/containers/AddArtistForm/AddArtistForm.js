import React, {useState} from "react";

const AddArtistForm = ({onSubmit}) => {

    const [state, setState] = useState({
        title: "",
        information: "",
        image: ""
    });

    const submitFormHandler = e => {
        e.preventDefault();
        let formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });
        onSubmit(formData);
    };
    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };
    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => ({...prevState, [name]: file}));
    };

    return (
        <div className="container bg-gradient-info">
            <form
                className="p-5"
                onSubmit={submitFormHandler}
            >
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1">Artist Name</span>
                    </div>
                    <input type="text"
                           className="form-control"
                           id="title-artist"
                           placeholder="Enter artist name"
                           value={state.title}
                           onChange={inputChangeHandler}
                           name="title"
                           required/>
                </div>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text">Artist information</span>
                    </div>
                    <textarea className="form-control"
                              placeholder="Add information about album"
                              onChange={inputChangeHandler}
                              name="information"
                              value={state.information}
                              required
                              id="information-artist"
                    />
                </div>
                <div className="input-group mb-3 ">
                    <label htmlFor="image-artist">Add file</label>
                    <input
                        type="file"
                        name="image"
                        className="form-control-file"
                        onChange={fileChangeHandler}
                        id="image-artist"
                    />
                </div>
                <button
                    type="submit"
                    className="btn btn-info"
                    id="btn-artist"
                >Create new artist
                </button>
            </form>
        </div>
    );
};

export default AddArtistForm;