import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Layout from "./components/Layout/Layout";
import ArtistsList from "./components/ArtistsList/ArtistsList";
import AlbumsList from "./components/AlbumList/AlbumsList";
import TracksList from "./components/TracksList/TracksList";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import {useSelector} from "react-redux";
import TrackHistory from "./components/TrackHistory/TrackHistory";
import NewArtist from "./components/NewArtist/NewArtist";
import NewAlbum from "./components/NewAlbum/NewAlbum";
import NewTrack from "./components/NewTrack/NewTrack";

const App = () => {
    const user = useSelector(state => state.users.user)
    return (
        <Layout user={user}>
            <Switch>
                <Route path="/" exact component={ArtistsList}/>
                <Route path="/albums/:id" exact component={AlbumsList}/>
                <Route path="/tracks/:id" exact component={TracksList}/>
                <Route path="/register" exact component={Register}/>
                <Route path="/login" exact component={Login}/>
                <Route path="/trackHistory" exact component={TrackHistory}/>
                <Route path="/add/new_artist" exact component={NewArtist}/>
                <Route path="/add/new_album" exact component={NewAlbum}/>
                <Route path="/add/new_track" exact component={NewTrack}/>
                <Route render={() => <h1>404 Not Found</h1>}/>
            </Switch>
        </Layout>
    );
}

export default App;
